package ooss;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Klass {
    private int number;
    private Student leader;

    private List<Person> attachPersons;

    public Klass(int number) {
        this.number = number;
        attachPersons = new ArrayList<>();
    }

    public int getNumber() {
        return number;
    }

    public void assignLeader(Student student) {
        if (student != null && student.getKlass() != null && student.getKlass().getNumber() == number) {
            leader = student;
            attachPersons.forEach(person -> person.say(number,leader));
        } else {
            System.out.println("It is not one of us.");
        }
    }

    public boolean isLeader(Student student) {
        return leader.equals(student);
    }

    public void attach(Person person) {
        attachPersons.add(person);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Klass klass = (Klass) o;
        return number == klass.number;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }
}
