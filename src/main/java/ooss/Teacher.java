package ooss;

import java.util.ArrayList;
import java.util.List;

public class Teacher extends Person {
    private List<Klass> klassList;

    public Teacher(int id, String name, int age) {
        super(id, name, age);
        klassList = new ArrayList<>();
    }

    @Override
    public String introduce() {
        String intro = String.format("My name is %s. I am %d years old. I am a teacher.", name, age);
        if (!klassList.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            sb.append(" I teach Class ");
            klassList.forEach(klass -> sb.append(klass.getNumber()).append(", "));
            sb.delete(sb.length()-2,sb.length());
            sb.append(".");
            intro += sb;
        }
        return intro;
    }

    public void assignTo(Klass klass) {
        klassList.add(klass);
    }

    public boolean belongsTo(Klass klass) {
        return klassList.contains(klass);
    }

    public boolean isTeaching(Student student) {
        return klassList.contains(student.getKlass());
    }

    public void say(int classNumber, Student leader) {
        System.out.println(String.format("I am %s, teacher of Class %d. I know %s become Leader.",name,classNumber,leader.getName()));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Teacher teacher = (Teacher) o;
        return this.id == teacher.id;
    }
}
