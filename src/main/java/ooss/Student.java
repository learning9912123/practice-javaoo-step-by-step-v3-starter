package ooss;

public class Student extends Person {
    private Klass klass;

    public Student(int id, String name, int age) {
        super(id, name, age);
    }

    @Override
    public String introduce() {
        String intro = String.format("My name is %s. I am %d years old. I am a student.", name, age);
        if (klass != null) {
            if (klass.isLeader(this)){
                intro += String.format(" I am the leader of class %d.", klass.getNumber());
            }else {
                intro += String.format(" I am in class %d.", klass.getNumber());
            }
        }
        return intro;
    }

    public void introduce(String belonging) {
        System.out.println(String.format("My name is %s. I am %d years old. I am a student.%s", name, age, belonging));
    }

    public void join(Klass klass) {
        this.klass = klass;
    }

    public boolean isIn(Klass klass) {
        return this.klass != null && this.klass.equals(klass);
    }

    public void say(int classNumber,Student leader) {
        System.out.println(String.format("I am %s, student of Class %d. I know %s become Leader.",name,classNumber,leader.getName()));
    }

    public Klass getKlass() {
        return klass;
    }

    public void setKlass(Klass klass) {
        this.klass = klass;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return this.id == student.id;
    }
}
